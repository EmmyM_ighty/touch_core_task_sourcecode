const express = require("express");
const router = express.Router();
const db = require("../db");

// Get all Events
router.get("/events", async (req, res, next) => {
  try {
    const results = await db.query(
      "SELECT * FROM event e JOIN actor a ON e.id = a.event_id JOIN repo r ON e.id = r.event_id ORDER BY e.id"
    );

    const result = results.rows.map((result) => {
      let singleResult = {
        id: result.id,
        type: result.type,
        actor: {
          id: result.actorid,
          login: result.login,
          avatar_url: result.avatar_url,
        },
        repo: {
          id: result.repoid,
          name: result.name,
          url: result.url,
        },
        created_at: result.createdat,
      };

      return singleResult;
    });

    return res.json(result);
  } catch (err) {
    console.log(err);
    return next(err);
  }
});

// Add an event
router.post("/events", async (req, res, next) => {
  try {
    const result = await db
      .query(
        "INSERT INTO event (id, type, createdat, actor_id, repo_id) VALUES ($1, $2, $3, $4, $5) RETURNING *",
        [
          req.body.id,
          req.body.type,
          req.body.created_at,
          req.body.actor.id,
          req.body.repo.id,
        ]
      )
      .then(() => {
        db.query(
          "INSERT INTO actor (actorid, login, avatar_url, event_id) VALUES ($1, $2, $3, $4) RETURNING *",
          [
            req.body.actor.id,
            req.body.actor.login,
            req.body.actor.avatar_url,
            req.body.id,
          ]
        );
      })
      .then(() => {
        db.query(
          "INSERT INTO repo (repoid, name, url, event_id) VALUES ($1, $2, $3, $4) RETURNING *",
          [req.body.repo.id, req.body.repo.name, req.body.repo.url, req.body.id]
        );
      });

    return res.status(200).json({
      message: "Successfully Added",
    });
  } catch (err) {
    return next(err);
  }
});

//Erasing all events
router.delete("/erase", async (req, res, next) => {
  try {
    await db.query("TRUNCATE event, actor, repo CASCADE");

    return res.status(200).json({
      message: "Successfully Erased",
    });
  } catch (err) {
    return next(err);
  }
});

//Return the event records filtered by the actor ID
router.get("/events/actors/:actorID", async (req, res, next) => {
  try {
    const results = await db.query(
      "SELECT * FROM event e JOIN actor a ON e.id = a.event_id JOIN repo r ON e.id = r.event_id WHERE a.actorid=$1 ORDER BY e.id ASC",
      [req.params.actorID]
    );

    if (results.rows.length > 0) {
      const result = results.rows.map((result) => {
        let singleResult = {
          id: result.id,
          type: result.type,
          actor: {
            id: result.actorid,
            login: result.login,
            avatar_url: result.avatar_url,
          },
          repo: {
            id: result.repoid,
            name: result.name,
            url: result.url,
          },
          created_at: result.createdat,
        };

        return singleResult;
      });

      return res.status(200).json(result);
    } else {
      return res.status(400).json({
        message: "No such actor exists!",
      });
    }
  } catch (err) {
    return next(err);
  }
});

//Updating the avatar URL of the actor
router.put("/actors", async (req, res, next) => {
  try {
    const result = await db.query(
      "UPDATE actor SET avatar_url=$1 WHERE actorid=$2 RETURNING *",
      [req.body.avatar_url, req.body.id]
    );

    if (result.rows[0]) {
      return res.status(200).json(result.rows[0]);
    } else {
      return res.status(400).json({
        message: "That user does not exist!",
      });
    }
  } catch (err) {
    return next(err);
  }
});

// Returning the actor records ordered by the total number of events
router.get("/actors", async (req, res, next) => {
  try {
    const result = await db.query(
      "select a.actorid, a.login, a.avatar_url from event e join actor a on e.actor_id=a.actorid GROUP BY a.actorid ORDER BY COUNT(a.actorid) DESC"
    );

    const results = result.rows.map((res) => {
      const formatedResult = {
        id: res.actorid,
        login: res.login,
        avatar_url: res.avatar_url,
      };

      return formatedResult;
    });

    return res.status(200).json(results);
  } catch (err) {
    return next(err);
  }
});

module.exports = router;
