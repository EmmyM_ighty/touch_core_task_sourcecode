const express = require("express");
const app = express();
const cors = require("cors");

const eventsRoutes = require("./routes/events");

app.use(express.json());
app.use(cors());

app.use("/", eventsRoutes);

app.use((req, res, next) => {
  let err = new Error("Not Found!");
  err.status = 404;
  next(err);
});

if (app.get("env") === "development") {
  app.use((err, req, res, next) => {
    res.status(err.status || 500);
    res.send({
      message: err.message,
      error: err,
    });
  });
}

app.listen(3000, () => {
  console.log("Server is running...");
});
